#ifndef DIALOG_CREDITS_H
#define DIALOG_CREDITS_H

#include <QDialog>

namespace Ui {
class Dialog_Credits;
}

class Dialog_Credits : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog_Credits(QWidget *parent = nullptr);
    ~Dialog_Credits();

private slots:
    void on_pushButton_2_clicked();

private:
    Ui::Dialog_Credits *ui;

    QString _browser = "/usr/bin/firefox";

};

#endif // DIALOG_CREDITS_H

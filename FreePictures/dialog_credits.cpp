#include "dialog_credits.h"
#include "ui_dialog_credits.h"

Dialog_Credits::Dialog_Credits(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog_Credits)
{
    ui->setupUi(this);
    // textBrowser
    QString output = "Thank you all for your time and effort in all that you have done.<br>";
    output = output + "Program Created by: Asher Simcha" + "<br>";
    output = output + "2022" + "<br>";
    output = output + "" + "<br>";
    output = output + "Background Picture by:" + "<br>";
    output = output + "Pawel Czerwinski" + "<br>";
    output = output + "https://unsplash.com/photos/Rj8pv09tU5A" + "<br>";
    output = output + "" + "<br>";
    output = output + "Logo by:" + "<br>";
    output = output + "Asher Simcha" + "<br>";
    output = output + "Copyright 2022 All rights reserved, not free to use." + "<br>";
    output = output + "" + "<br>";
    output = output + "Programs Used:" + "<br>";
    output = output + "QT" + "<br>";
    output = output + "GIMP" + "<br>";
    output = output + "Firefox" + "<br>";
    output = output + "Chrome" + "<br>";
    output = output + "MX Linux" + "<br>";
    output = output + "Ubuntu" + "<br>";
    ui->textBrowser->setText(output);
}

Dialog_Credits::~Dialog_Credits()
{
    delete ui;
}

void Dialog_Credits::on_pushButton_2_clicked()
{
    QString output = _browser + " www.gitlab.com/GregoryWerds/freepictures/  &";
    QByteArray out = output.toLocal8Bit();
    system(out);
}

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "QMessageBox"
#include "dialog_credits.h"
#include "dialog_about.h"

//Here's a simple example implementation:

//#include <string>
//#include <vector>
//#include <sstream>
//#include <utility>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_creative_clicked()
{
    QString output;
    // creativecommons.org
    // search is: https://wordpress.org/openverse/search/image?q=background%20picture%20login
    QString Search = ui->lineEditSearch->text();
    if(Search != NULL){
        Search.replace(" ", "%20");
        output=_browser+" https://wordpress.org/openverse/search/image?q="+Search+" &";
    } else {
        output=_browser + " creativecommons.org &";
    }
    QByteArray out = output.toLocal8Bit();
    system(out);
}

void MainWindow::on_pushButton_vintage_clicked()
{
    // search is: https://vintagestockphotos.com/search.php?search=background+picutre+login
    QString Search = ui->lineEditSearch->text();
    if(Search != NULL){
        Search.replace(" ", "+");
        output=_browser+" https://vintagestockphotos.com/search.php?search="+Search+" &";
    } else {
        output=_browser + " vintagestockphotos.com &";
    }
    QByteArray out = output.toLocal8Bit();
    system(out);
}

void MainWindow::on_pushButton_pixabay_clicked()
{
    // search is: https://pixabay.com/images/search/background%20picture%20login/
    QString Search = ui->lineEditSearch->text();
    if(Search!=NULL){
        Search.replace(" ", "%20");
        output=_browser+" https://pixabay.com/images/search/"+Search+"/ &";
    } else {
        output=_browser + " pixabay.com &";
    }
    QByteArray out = output.toLocal8Bit();
    system(out);
}

void MainWindow::on_pushButton_wikimedia_clicked()
{
    // https://commons.wikimedia.org/wiki/Main_Page
    // Search: https://commons.wikimedia.org/w/index.php?search=background+woods&title=Special:MediaSearch&go=Go&type=image
    QString Search = ui->lineEditSearch->text();
    if(Search!=NULL){
        Search.replace(" ", "+");
        output=_browser+" https://commons.wikimedia.org/w/index.php?search="+Search+"&title=Special:MediaSearch&go=Go&type=image &";
    } else {
        output=_browser + " https://commons.wikimedia.org/wiki/Main_Page &";
    }
    QByteArray out = output.toLocal8Bit();
    system(out);
}

void MainWindow::on_pushButton_flickr_clicked()
{
    // www.flickr.com
    // Search: https://www.flickr.com/search/?text=about%20us%20icon&license=2%2C3%2C4%2C5%2C6%2C9
    output=_browser + " www.flickr.com &";
    QByteArray out = output.toLocal8Bit();
    system(out);
}

void MainWindow::on_pushButton_unsplash_clicked()
{
    // https://unsplash.com
    // Search is: https://unsplash.com/s/photos/background-login-images
    // run an external program
    // Search: https://unsplash.com/s/photos/backgound-woods
    QString Search = ui->lineEditSearch->text();
    if(Search!=NULL){
        Search.replace(" ", "+");
        output=_browser+" https://unsplash.com/s/photos/"+Search+" &";
    } else {
        output=_browser + " https://unsplash.com/ &";
    }
    QByteArray out = output.toLocal8Bit();
    system(out);
}

void MainWindow::on_pushButton_pexel_clicked()
{
    // Search: https://www.pexels.com/search/background%20woods/
    QString Search = ui->lineEditSearch->text();
    if(Search!=NULL){
        Search.replace(" ", "%20");
        output=_browser+" https://www.pexels.com/search/"+Search+"/ &";
    } else {
        output=_browser + " https://pexels.com &";
    }
    QByteArray out = output.toLocal8Bit();
    system(out);
}

void MainWindow::on_pushButton_lifeofpix_clicked()
{
    // Search: https://www.lifeofpix.com/search/background%20woods?
    QString Search = ui->lineEditSearch->text();
    if(Search!=NULL){
        Search.replace(" ", "%20");
        output=_browser+" https://www.lifeofpix.com/search/"+Search+"? &";
    } else {
        output=_browser + " www.lifeofpix.com &";
    }
    QByteArray out = output.toLocal8Bit();
    system(out);
}

void MainWindow::on_pushButton_stocksnap_clicked()
{
    // Search:  https://stocksnap.io/search/background+woods
    QString Search = ui->lineEditSearch->text();
    if(Search!=NULL){
        Search.replace(" ", "+");
        output=_browser+" https://stocksnap.io/search/"+Search+" &";
    } else {
        output=_browser + " https://stocksnap.io &";
    }
    QByteArray out = output.toLocal8Bit();
    system(out);
}

void MainWindow::on_pushButton_burst_clicked()
{
    // Search: https://burst.shopify.com/photos/search?utf8=%E2%9C%93&q=backgrounds+trees&button==
    QString Search = ui->lineEditSearch->text();
    if(Search!=NULL){
        Search.replace(" ", "+");
        output=_browser+" https://burst.shopify.com/photos/search?q="+Search+"&button= &";
    } else {
        output=_browser + " https://burst.shopify.com/photos &";
    }
    QByteArray out = output.toLocal8Bit();
    system(out);
}

void MainWindow::on_pushButton_kaboompics_clicked()
{
    // Search: https://kaboompics.com/gallery?search=background+woods
    QString Search = ui->lineEditSearch->text();
    if(Search!=NULL){
        Search.replace(" ", "+");
        output=_browser+" https://kaboompics.com/gallery?search="+Search+" &";
    } else {
        output=_browser + " https://kaboompics.com/ &";
    }
    QByteArray out = output.toLocal8Bit();
    system(out);
}

void MainWindow::on_pushButton_gratisography_clicked()
{
    // https://gratisography.com/
    // Search: https://gratisography.com/?s=background+woods
    QString Search = ui->lineEditSearch->text();
    if(Search!=NULL){
        Search.replace(" ", "+");
        output=_browser+" https://gratisography.com/?s="+Search+" &";
    } else {
        output=_browser + " https://gratisography.com/ &";
    }
    QByteArray out = output.toLocal8Bit();
    system(out);
}

void MainWindow::on_pushButton_libreshot_clicked()
{
    // Search: https://libreshot.com/?s=background+woods
    QString Search = ui->lineEditSearch->text();
    if(Search!=NULL){
       Search.replace(" ", "+");
       output=_browser+" https://libreshot.com/?s="+Search+" &";
    } else {
        output=_browser + " https://libreshot.com/ &";
    }
    QByteArray out = output.toLocal8Bit();
    system(out);
}

void MainWindow::on_pushButton_illustrations_clicked()
{
    // Search: https://illustrations.co/designs/background-woods
    QString Search = ui->lineEditSearch->text();
    if(Search!=NULL){
        Search.replace(" ", "-");
        output=_browser + " https://illustrations.co/designs/"+Search+" &";

    }else{
        output=_browser + " https://illustrations.co/ &";
    }
    QByteArray out = output.toLocal8Bit();
    system(out);
}

void MainWindow::on_pushButton_blushdesign_clicked()
{
    //output=_browser + " https://www.openpeeps.com/ &";
    // Search: https://blush.design/search/background-woods/
    QString Search = ui->lineEditSearch->text();
    if(Search!=NULL){
        Search.replace(" ", "-");
        output=_browser + " https://blush.design/search/"+Search+" &";

    }else{
        output=_browser + " https://blush.design/search/ &";
    }
    QByteArray out = output.toLocal8Bit();
    system(out);
}

void MainWindow::on_pushButton_artvee_clicked()
{
    // very dificult to understand
    // Search: https://artvee.com/main/?s=background+woods&tc=pd
    //output=_browser + " https://artvee.com &";
    QString Search = ui->lineEditSearch->text();
    if(Search!=NULL){
        Search.replace(" ", "+");
        output=_browser + " https://artvee.com/main/?s="+Search+"&tc=pd &";
    }else{
      output=_browser + " https://artvee.com &";
    }
    QByteArray out = output.toLocal8Bit();
    system(out);
}

void MainWindow::on_pushButton_freeillustrations_clicked()
{
    // Search: http://freeillustrations.xyz/?s=background+woods
    QString Search = ui->lineEditSearch->text();
    if(Search!=NULL){
        Search.replace(" ", "+");
        output=_browser + " http://freeillustrations.xyz/?s="+Search+" &";
    }else{
        output=_browser + " http://freeillustrations.xyz &";
    }
    QByteArray out = output.toLocal8Bit();
    system(out);
}

void MainWindow::on_actionCredits_triggered()
{
    Dialog_Credits dialog_credits;
    dialog_credits.setModal(true);
    dialog_credits.exec();
}

void MainWindow::on_actionAbout_triggered()
{
    Dialog_About dialog_about;
    dialog_about.setModal(true);
    dialog_about.exec();
}

void MainWindow::on_pushButton_logo_clicked()
{
    QString output = _browser + " www.gitlab.com/GregoryWerds/freepictures/  &";
    QByteArray out = output.toLocal8Bit();
    system(out);
}
